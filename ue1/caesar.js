var cipheredText = "";
var plainText = "";
var shift = 0;

function encrypt() {
  // Usereingabe starten
  promptInputFromUser();

  // nur verschluesseln, wenn Text und Shift OK
  if (plainText.length > 0 && shift > 0) {
    cipheredText = cipherText(plainText);
    alert(`The ciphered text: ${cipheredText}`);
  }
}

function decrypt() {
  // decrypt ist encrypt mit negiertem Shift
  shift *= -1;
  if (cipheredText.length > 0 && shift < 0) {
    plainText = cipherText(cipheredText);
    alert(`The plain text was: ${plainText}`);
  }
}

function cipherChar(charCode) {
  // char code um shift Zeichen versetzen
  var transformedCharCode = charCode + shift;
  console.error(transformedCharCode);
  // Zeichen muss nach Transformation in A-Za-z bleiben
  if (transformedCharCode > 126) {
    transformedCharCode -= 95;
  } else if (transformedCharCode < 32) {
    transformedCharCode += 95;
  }

  return transformedCharCode;
}

function cipherText(plainTextToCipher) {
  var result = "";

  // über alle Chars loopen
  for (var i = 0; i < plainTextToCipher.length; i++) {
    //
    var actualCharCode = plainTextToCipher.charCodeAt(i);

    // Verschluesseln eines Zeichens
    actualCharCode = cipherChar(actualCharCode);

    // zum Code korrespondierendes Zeichen anhaengen
    result += String.fromCharCode(actualCharCode);
  }

  return result;
}

function promptInputFromUser() {
  plainText = prompt("Plain text");
  shift = prompt("Shift");
  // wichtig: prompt() liefert immer String -> Parsen
  shift = parseInt(shift);
}
