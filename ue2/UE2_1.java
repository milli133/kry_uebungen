package ue2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class UE2_1 {

    private static void MD5(String text) throws NoSuchAlgorithmException {
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.reset();
        m.update(text.getBytes(StandardCharsets.UTF_8));
        byte[] digest = m.digest();

        StringBuilder sb = new StringBuilder();
        for (byte b : digest) {
            sb.append(String.format("%02x", b));
        }
        System.out.println("MD5 Hash: " + sb.toString());
    }

    private static void SHA256(String text, boolean useSalt) throws NoSuchAlgorithmException {

        MessageDigest m = MessageDigest.getInstance("SHA-256");
        m.reset();
        m.update(text.getBytes(StandardCharsets.UTF_8));

        // Wenn die Salt-Option gewählt wurde, wird Salt generiert und dem Digest
        // mitgegeben
        if (useSalt) {
            SecureRandom random = new SecureRandom();
            byte[] salt = new byte[16];
            random.nextBytes(salt);
            m.update(salt);
        }

        byte[] digest = m.digest();
        StringBuilder sb = new StringBuilder();
        for (byte b : digest) {
            sb.append(String.format("%02x", b));
        }
        System.out.println("SHA-256 Hash: " + sb.toString());
    }

    private static String BCrypt(String text) {
        // Library jBCrypt implementiert den BCrypt-Alogithmus -> Klasse BCrypt.java
        // Source: https://github.com/jeremyh/jBCrypt
        // BCrypt wurde als Hash Funktion speziell für den Einsatz als Passwort-Hash
        // entwickelt
        // BCrypt basiert auf dem Blowfish-Algorithmus und erweitert diesen um
        // Rundenschlüssel und generierten S-Boxen
        // Diese Runden können 2^n betragen, wobei n die cost ist (wird exponentiell pro
        // Runde "teurer")
        // Ergebnis von BCrypt ist ein 60 Zeichen String der von einem $-Zeichen
        // eingeleitet wird
        // Salt ist auch essenzieller Teil des BCrypt und ist mittels spezieller Base64
        // Kodierung implementiert
        String hash = BCrypt.hashpw(text, BCrypt.gensalt(12));

        System.out.println("BCrypt Hash: " + hash);
        return hash;

    }

    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter PW");
        try {
            String text = br.readLine();
            MD5(text);
            // einmal mit und einmal ohne Salt
            // man kann die Ergebnisse dann vergleichen
            SHA256(text, false);
            SHA256(text, true);
            String hashB = BCrypt(text);
            System.out.println(hashB);
            // PW Check
            // BCrypt-Library bietet auch die Möglichkeit den eingegebenen Hash nochmal zu
            // überprüfen
            br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter last PW to check");
            if (BCrypt.checkpw(br.readLine(), hashB))
                System.out.println("Match :)");
            else
                System.out.println("No match :(");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
