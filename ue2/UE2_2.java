package ue2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class UE2_2 {

    // true - wenn match weiter Rainbow Tables anlegen und befüllen
    // false - bei match keine Einträge mehr anlegen und auch keine weiteren
    // Tabellen anlegen
    private static Boolean continueWritingTablesAfterMatch = false;

    private static Boolean match = false;
    private static String matchAlgorithm = "";
    private static String matchPW = "";

    // Schreibt eine Rainbow-Table-Datei für den spezifizierten Algorithmus
    // vergleicht den übergebenen Hash hashPW mit dem
    // Hash der in der Zeile der best110 ausgelesenen Passworts
    // true = Hash matched
    // false = kein Match
    private static void findPW(String hashPW, String hashType) {

        MessageDigest md;
        try {
            BufferedReader br = new BufferedReader(new FileReader("ue2/best110.txt"));
            // File für Rainbow Table
            FileWriter fw = new FileWriter(new File("ue2/rainbow_tables/" + "rainbow_" + hashType + ".txt"));
            String line = "";

            while ((line = br.readLine()) != null) {
                md = MessageDigest.getInstance(hashType);
                md.reset();
                md.update(line.getBytes(StandardCharsets.UTF_8));
                byte[] h = md.digest();
                // Trugschluss: hString = h.toString() liefert nicht das Byte-Array als
                // Hex-String
                StringBuilder sb = new StringBuilder();
                for (byte b : h) {
                    sb.append(String.format("%02x", b));
                }
                String hString = sb.toString();
                // Zeile in den Rainbow-Table speichern
                fw.write(hString);
                fw.write(System.lineSeparator());
                fw.flush();

                // Vergleich der Hashes
                if (hString.equals(hashPW)) {
                    match = true;
                    matchPW = line;
                    matchAlgorithm = hashType;

                    // siehe Beschreibung in der Klasse
                    if (!continueWritingTablesAfterMatch) {
                        br.close();
                        fw.close();
                        return;
                    }
                    continue;
                }
            }
            br.close();
            fw.close();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return;
    }

    public static void main(String[] args) {

        // PW einlesen
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter PW");
        String hashPW = "";
        try {
            hashPW = br.readLine();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        if (hashPW.length() < 6) {
            System.out.println("PW too short!");
            return;
        }
        // hier definiert man die Algorithmen, auf die abgeprüft werden soll
        final String hashTypes[] = { "MD5", "SHA-224", "SHA-256", "SHA-512" };

        // alle definierten Hash-Algorithmen durchprobieren
        for (String hashType : hashTypes) {
            findPW(hashPW, hashType);
        }

        // Wenn gefunden - Ausgabe der Infos über den Algorithmus sowie das Passwort im
        // Plaintext
        if (match) {
            System.out.println("Match! Algorithm: " + matchAlgorithm);
            System.out.println("Plain PW: " + matchPW);
        } else
            System.out.println("No matching PW found :(");
    }
}
